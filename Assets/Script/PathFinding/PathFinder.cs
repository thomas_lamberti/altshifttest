using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PathFinder<T> where T : IPathfindable
{
    public static Path<T> FindPath(
        T start,
        T destination)
    {
        var closed = new HashSet<T>();
        var queue = new PriorityQueue<double, Path<T>>();
        queue.Enqueue(0, new Path<T>(start));

        while (!queue.IsEmpty)
        {
            var path = queue.Dequeue();

            if (closed.Contains(path.LastStep))
                continue;
            if (path.LastStep.Equals(destination))
                return path;

            closed.Add(path.LastStep);

            foreach (T n in path.LastStep.GetNeightboor())
            {
                double d = distance(path.LastStep, n);
                var newPath = path.AddStep(n, d);
                queue.Enqueue(newPath.TotalCost + estimate(n, destination), 
                    newPath);
            }
        }

        return null;
    }

    static double distance(T tile1, T tile2)
    {
        return 1;
    }

    static double estimate(T tile, T destTile)
    {
        float dx = Mathf.Abs(destTile.GetX() - tile.GetX());
        float dy = Mathf.Abs(destTile.GetY() - tile.GetY());
        float z1 = -(tile.GetX() + tile.GetY());
        float z2 = -(destTile.GetX() + destTile.GetY());
        float dz = Mathf.Abs(z2 - z1);

        return Mathf.Max(dx, dy, dz);
    }
}