﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameLogic : MonoBehaviour {

    public int m_Score = 0;
    public static int m_HighScore = 0;
    public Player m_Player = null;
    public HexagonalMap m_Map=null;
    public TextMesh m_TxtScore = null;
    public TextMesh m_TxtHighScore = null;
    public TextMesh m_TxtMessage = null;
    public GameObject m_RetryPanel = null;
    public AudioClip m_SndBonusCollected = null;
    public AudioClip m_SndWin = null;
    public AudioClip m_SndLoose = null;

    // Use this for initialization
    void Start ()
    {
        //Refresh highscore
        m_TxtHighScore.text = "HighScore : " + m_HighScore.ToString() + "/20";
        //Map creation
        m_Map.InitMap();
    }

    public void OnUserClickOnTile(HexagonalTile _tile)
    {
        m_Player.SetNewTargetForPlayer(_tile);
    }

    public void PlayerWin()
    {
        m_TxtMessage.text = "Congratulations !";
        m_RetryPanel.SetActive(true);
        PlaySound(m_SndWin);
    }

    public bool CheckPlayerLoose(HexagonalTile _tile)
    {
     
        if (_tile.GetNeightboor().Count == 0)
        {
            m_TxtMessage.text = "You're trapped !";
            m_RetryPanel.SetActive(true);
            PlaySound(m_SndLoose);
            return true;
        }

        return false;
    }

    public void OnPlayerCollectBonus(HexagonalTile _tile)
    {
        m_Score += _tile.m_Bonus.m_Point;
        PlaySound(m_SndBonusCollected);

        GameObject.Destroy(_tile.m_Bonus.gameObject);

        // We refresh the score only when there is changes.
        m_TxtScore.text = "Score : " + m_Score.ToString() + "/20";
        m_TxtHighScore.text = "HighScore : " + m_HighScore.ToString() + "/20";

        m_HighScore = Mathf.Max(m_HighScore, m_Score);

        if (m_Score == 20)
        {
            PlayerWin();
        }
    }

    private void PlaySound(AudioClip _clip)
    {
        AudioSource audio= Camera.main.GetComponent<AudioSource>();
        audio.clip = _clip;
        audio.Play();
    }
}
