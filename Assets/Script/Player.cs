﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public List<HexagonalTile> m_LTarget = new List<HexagonalTile>();
    public GameLogic m_Logic = null;
    public HexagonalTile m_CurrentTile = null;

    // Update is called once per frame
    void Update()
    {
        if (m_LTarget.Count != 0)
        {
            if (m_LTarget[0] == null)
                return;

            // This code make the ship move to the next target.
            Vector3 subtarget = m_LTarget[0].transform.position;
            subtarget.z = transform.position.z;

            Vector3 desiredspeed = subtarget - transform.position;

            float lenght = desiredspeed.magnitude;
            float realspeed = Mathf.Min(lenght, 1f * Time.deltaTime);

            transform.position = transform.position + (desiredspeed.normalized * realspeed);

            //Considering we are arrived, we are removing the last tile.
            if (lenght < 0.001f)
            {
                m_Logic.m_Map.RemoveTile(m_CurrentTile);

                m_CurrentTile = m_LTarget[0];

                if (m_Logic.CheckPlayerLoose(m_CurrentTile) == true)
                {
                    return;
                }

                CheckBonus(m_LTarget[0]);
                m_LTarget.RemoveAt(0);
            }
        }
    }

    public void CheckBonus(HexagonalTile _tile)
    {
        if (_tile.m_Bonus != null)
        {
            m_Logic.OnPlayerCollectBonus(_tile);
        }
    }

    public void ForcePositionToCurrentTile()
    {
        Vector3 pos = m_CurrentTile.transform.position;
        pos.z = transform.position.z;
        transform.position = pos;

    }

    public void SetNewTargetForPlayer(HexagonalTile _tile)
    {
        if (m_LTarget.Count != 0)
            return;
        if (m_Logic.m_RetryPanel.gameObject.activeInHierarchy == true)
            return;

        IPathfindable PathStart = (IPathfindable)m_CurrentTile;
        IPathfindable PathEnd = (IPathfindable)_tile;

        Path<IPathfindable> path = PathFinder<IPathfindable>.FindPath(PathStart, PathEnd);

        if (path == null)
        {
            return;
        }

        AssignNewPathToPlayer(path);
    }

    private void AssignNewPathToPlayer(Path<IPathfindable> path)
    {
        var PathEnumerator = path.GetEnumerator();
        PathEnumerator.MoveNext();

        do
        {
            IPathfindable p = PathEnumerator.Current;
            GameObject gameObject1 = ((HexagonalTile)p).gameObject;
            m_LTarget.Insert(0, ((HexagonalTile)p));
        } while (PathEnumerator.MoveNext());
    }

}
