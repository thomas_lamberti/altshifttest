﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HexagonalTile : MonoBehaviour,IPathfindable
{
    public List<HexagonalTile> m_Neighboor = new List<HexagonalTile>();
    public HexagonalBonus m_Bonus;
    public GameLogic m_Logic = null;

    void OnMouseDown()
    {
        m_Logic.OnUserClickOnTile(this);
    }

    public float GetX()
    {
        return transform.position.x;
    }
    public float GetY()
    {
        return transform.position.y;
    }
    public List<IPathfindable> GetNeightboor()
    {
        return m_Neighboor.OfType<IPathfindable>().ToList();
    }

}

public interface IPathfindable
{
    float GetX();
    float GetY();
    List<IPathfindable> GetNeightboor();
}