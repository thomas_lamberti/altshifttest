﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HexagonalMap : MonoBehaviour {
    public GameObject m_Tile = null;
    public GameObject m_Bonus = null;

    public Vector3 m_SpaceBetweenTile = new Vector3(1.15f, 1.04f, 0);
    public List<HexagonalTile> m_LTile = new List<HexagonalTile>();
    public GameLogic m_Logic = null;

    //Method for map creation
    public void InitMap()
    {
        for(int x=0;x<11;x++)
            for (int y = 0; y < 9; y++)
            {

                if (((x % 2 == 1) && (y % 2 == 1))==false)
                {
                    //Randomly some tiles are not drawn to make the game more challenging.
                    if(Random.value<0.1f)
                    continue;
                }

                float xoffset = 0;

                if (y % 2 == 0)
                    xoffset = m_SpaceBetweenTile.x / 2f;

                //Show tiles
                GameObject GOTile = (GameObject) GameObject.Instantiate(m_Tile, this.transform);
                GOTile.transform.position = new Vector3(x*m_SpaceBetweenTile.x+ xoffset, y*m_SpaceBetweenTile.y, 0);
                HexagonalTile CurrentTile = GOTile.GetComponent<HexagonalTile>(); 
                CurrentTile.m_Logic = m_Logic;
                m_LTile.Add(CurrentTile);

                if ((x%2==1)&&(y%2==1))
                {   
                    //Show Bonus
                    GameObject GOBonus = (GameObject)GameObject.Instantiate(m_Bonus, this.transform);
                    GOBonus.transform.position = new Vector3(x * m_SpaceBetweenTile.x + xoffset, y * m_SpaceBetweenTile.y, 0);
                    CurrentTile.m_Bonus = GOBonus.GetComponent<HexagonalBonus>();
                }


                if ((x ==5) && (y ==5))
                {
                    //Setup the ship
                    m_Logic.m_Player.m_CurrentTile = CurrentTile;
                    m_Logic.m_Player.ForcePositionToCurrentTile();
                }
            }

        m_LTile.ForEach(x => ComputeNeightboorList(x));

    }


    public void RemoveTile(HexagonalTile _tile)
    {
        if (_tile == null)
            return;

        var neightboor = m_Logic.m_Player.m_CurrentTile.GetNeightboor().OfType<HexagonalTile>().ToList();
        m_LTile.Remove(_tile);
        GameObject.Destroy(m_Logic.m_Player.m_CurrentTile.gameObject);
        neightboor.ForEach(x => ComputeNeightboorList(x));
        
    }

    public void ComputeNeightboorList(HexagonalTile _Tile)
    {
        if (_Tile == null)
            return;

        float radius = m_SpaceBetweenTile.x * 1.1f;
        Vector3 TilePosition = _Tile.transform.position;
        _Tile.m_Neighboor=m_LTile.Where(x => (Vector3.Distance(TilePosition, x.transform.position) < radius)&&(x!=_Tile)).ToList();

    }
    public void RandomlyRemove(HexagonalTile _tile)
    {
        if(_tile.m_Bonus==null)
        if (Random.value < 0.5f)
            RemoveTile(_tile);
    }

}
